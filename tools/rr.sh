#!/bin/bash
# Root method chooser for the Resurrection Remix ROM
# Specify colors utilized in the terminal
    red=$(tput setaf 1)             #  red
    grn=$(tput setaf 2)             #  green
    ylw=$(tput setaf 3)             #  yellow
    blu=$(tput setaf 4)             #  blue
    ppl=$(tput setaf 5)             #  purple
    cya=$(tput setaf 6)             #  cyan
    txtbld=$(tput bold)             #  Bold
    bldred=${txtbld}$(tput setaf 1) #  red
    bldgrn=${txtbld}$(tput setaf 2) #  green
    bldylw=${txtbld}$(tput setaf 3) #  yellow
    bldblu=${txtbld}$(tput setaf 4) #  blue
    bldppl=${txtbld}$(tput setaf 5) #  purple
    bldcya=${txtbld}$(tput setaf 6) #  cyan
    txtrst=$(tput sgr0)             #  Reset
    rev=$(tput rev)                 #  Reverse color
    pplrev=${rev}$(tput setaf 5)
    cyarev=${rev}$(tput setaf 6)
    ylwrev=${rev}$(tput setaf 3)
    blurev=${rev}$(tput setaf 4)
    normal='tput sgr0'

                                                                                                              
tput setaf 4                                                                                                             
echo -e "ZZZZZZZZZZZZZZZZZZZ                    NNNNNNNN        NNNNNNNN  iiii          tttt      YYYYYYY       YYYYYYY";
echo -e "Z:::::::::::::::::Z                    N:::::::N       N::::::N i::::i      ttt:::t      Y:::::Y       Y:::::Y";
echo -e "Z:::::::::::::::::Z                    N::::::::N      N::::::N  iiii       t:::::t      Y:::::Y       Y:::::Y";
echo -e "Z:::ZZZZZZZZ:::::Z                     N:::::::::N     N::::::N             t:::::t      Y::::::Y     Y::::::Y";
echo -e "ZZZZZ     Z:::::Z      eeeeeeeeeeee    N::::::::::N    N::::::Niiiiiiittttttt:::::tttttttYYY:::::Y   Y:::::YYY";
echo -e "        Z:::::Z      ee::::::::::::ee  N:::::::::::N   N::::::Ni:::::it:::::::::::::::::t   Y:::::Y Y:::::Y   ";
echo -e "       Z:::::Z      e::::::eeeee:::::eeN:::::::N::::N  N::::::N i::::it:::::::::::::::::t    Y:::::Y:::::Y    ";
echo -e "      Z:::::Z      e::::::e     e:::::eN::::::N N::::N N::::::N i::::itttttt:::::::tttttt     Y:::::::::Y     ";
echo -e "     Z:::::Z       e:::::::eeeee::::::eN::::::N  N::::N:::::::N i::::i      t:::::t            Y:::::::Y      ";
echo -e "    Z:::::Z        e:::::::::::::::::e N::::::N   N:::::::::::N i::::i      t:::::t             Y:::::Y       ";
echo -e "   Z:::::Z         e::::::eeeeeeeeeee  N::::::N    N::::::::::N i::::i      t:::::t             Y:::::Y       ";
echo -e "ZZZ:::::Z     ZZZZZe:::::::e           N::::::N     N:::::::::N i::::i      t:::::t    tttttt   Y:::::Y       ";
echo -e "Z::::::ZZZZZZZZ:::Ze::::::::e          N::::::N      N::::::::Ni::::::i     t::::::tttt:::::t   Y:::::Y       ";
echo -e "Z:::::::::::::::::Z e::::::::eeeeeeee  N::::::N       N:::::::Ni::::::i     tt::::::::::::::tYYYY:::::YYYY    ";
echo -e "Z:::::::::::::::::Z  ee:::::::::::::e  N::::::N        N::::::Ni::::::i       tt:::::::::::ttY:::::::::::Y    ";
echo -e "ZZZZZZZZZZZZZZZZZZZ    eeeeeeeeeeeeee  NNNNNNNN         NNNNNNNiiiiiiii         ttttttttttt  YYYYYYYYYYYYY    ";                                                                                                          
echo -e "";
echo -e "";
echo -e "";
echo -e "";
echo -e ${ylw}" √ Build is Successfully Done! "${txtrst};
echo -e "";
tput setaf 1
echo -e " 〉Enjoy the ZeNiTy-GeNeraTion // #By ZeNiXxX ";
tput sgr0
echo -e "";
echo -e ${red}" 〉Enjoy the Resurrection Remix OS // #GETRESURRECTED "${txtrst};
echo -e "";
